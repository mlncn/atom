# Atom editor packages and configuration used by mlncn

Disclosure: I am far from a power user; i still turn to Vim more often than Atom.

Further disclosure: Haven't tested this to bring up a working Atom instance yet.  But these steps are [well](https://discuss.atom.io/t/installed-packages-list-into-single-file/12227/2) [reputed](https://stackoverflow.com/a/30006917/1028376https://stackoverflow.com/a/30006917/1028376) to work.

* Install [Atom](https://atom.io/)
* Replace your ~/.atom directory with this repository, or a fork of it.
* `apm install --packages-file ~/.atom/package-list.txt`

And now you should have an Atom editor set up with the same configuration as mine, useful for things like [writing in ASCII doc](http://data.agaric.com/writing-asciidoc-asciidoctor-easy-creation-book-quality-pdfs) or [generating PDFs from Markdown](http://data.agaric.com/creating-nicely-formatted-pdfs-from-markdown-files-atom-editor).
